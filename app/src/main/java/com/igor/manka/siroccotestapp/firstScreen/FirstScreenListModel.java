package com.igor.manka.siroccotestapp.firstScreen;

public class FirstScreenListModel {

    private int id;
    private String name;
    private int image;
    private int type;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getImage() {
        return image;
    }

    public void setImage(int image) {
        this.image = image;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }
}
