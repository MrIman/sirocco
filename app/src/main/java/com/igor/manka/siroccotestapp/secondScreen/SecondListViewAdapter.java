package com.igor.manka.siroccotestapp.secondScreen;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.igor.manka.siroccotestapp.R;

import java.util.ArrayList;
import java.util.Collections;

import butterknife.BindView;
import butterknife.ButterKnife;

public class SecondListViewAdapter extends ArrayAdapter<ArrayList<SecondScreenListModel>> {

    public static final int FIRST_ITEM_TYPE = 0;
    public static final int SECOND_ITEM_TYPE = 1;
    public static final int THIRD_ITEM_TYPE = 2;
    public static final int NORMAL_ITEM_TYPE = 3;


    private final Context context;
    private final ArrayList<SecondScreenListModel> data;
    private SecondListViewAdapterButtonsListener secondListViewAdapterButtonsListener;


    public SecondListViewAdapter(ArrayList<SecondScreenListModel> data, Context context, SecondListViewAdapterButtonsListener secondListViewAdapterButtonsListener) {
        super(context, 0, Collections.singletonList(data));
        this.context = context;
        this.data = data;
        this.secondListViewAdapterButtonsListener = secondListViewAdapterButtonsListener;
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        SecondScreenListModel secondScreenListModel = data.get(position);
        ViewHolder viewHolder = null;

        if (convertView == null) {
            LayoutInflater inflater = LayoutInflater.from(getContext());
            int itemViewType = getItemViewType(position);
            if (itemViewType == FIRST_ITEM_TYPE) {
                convertView = inflater.inflate(R.layout.first_row_second_screen, parent, false);
                setUpFirstItemViewHolder(convertView);
            } else if (itemViewType == SECOND_ITEM_TYPE) {
                convertView = inflater.inflate(R.layout.second_row_second_screen, parent, false);
                setUpSecondItemViewHolder(convertView);
            } else if (itemViewType == THIRD_ITEM_TYPE) {
                convertView = inflater.inflate(R.layout.third_row_second_screen, parent, false);
            } else {
                convertView = inflater.inflate(R.layout.row_item_second_screen, parent, false);
                viewHolder = new ViewHolder(convertView);
                viewHolder.itemImage = convertView.findViewById(R.id.rowItemFirstScreen_image);
                viewHolder.itemNumberTextView = convertView.findViewById(R.id.rowItemFirstScreen_text);
                convertView.setTag(viewHolder);
            }
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        if (getItemViewType(position) == NORMAL_ITEM_TYPE) {
            viewHolder.itemNumberTextView.setText("Item " + secondScreenListModel.getName());
            viewHolder.itemImage.setImageDrawable(ContextCompat.getDrawable(context, secondScreenListModel.getImage()));
        }

        return convertView;

    }

    private void setUpSecondItemViewHolder(View convertView) {
        SecondItemViewHolder secondItemViewHolder = new SecondItemViewHolder(convertView);
        secondItemViewHolder.button.setOnClickListener(this::onButtonClick);
    }

    private void setUpFirstItemViewHolder(View convertView) {
        FirstItemViewHolder firstItemViewHolder = new FirstItemViewHolder(convertView);
        firstItemViewHolder.button1.setOnClickListener(this::onFirstButtonClickListener);
        firstItemViewHolder.button2.setOnClickListener(this::onSecondButtonClickListener);
        firstItemViewHolder.button3.setOnClickListener(this::onThirdButtonClickListener);
    }

    private void onButtonClick(View view) {
        secondListViewAdapterButtonsListener.onButtonClick();
    }

    private void onThirdButtonClickListener(View view) {
        ((LinearLayout) view.getParent()).findViewById(R.id.first_row_button_1).setVisibility(View.INVISIBLE);
    }

    private void onSecondButtonClickListener(View view) {
        view.findViewById(R.id.first_row_button_2).setVisibility(View.GONE);
    }

    private void onFirstButtonClickListener(View view) {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getContext());
        alertDialogBuilder.setMessage(R.string.builder_button_text);
        alertDialogBuilder.setPositiveButton(R.string.dialog_confirm_text, null);
        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public int getViewTypeCount() {
        return 4;
    }

    @Override
    public int getItemViewType(int position) {
        return data.get(position).getType();
    }

    public interface SecondListViewAdapterButtonsListener {
        void onButtonClick();
    }

    public static class ViewHolder {
        @BindView(R.id.rowItemFirstScreen_image)
        ImageView itemImage;
        @BindView(R.id.rowItemFirstScreen_text)
        TextView itemNumberTextView;

        public ViewHolder(View convertView) {

        }
    }

    public static class FirstItemViewHolder {

        @BindView(R.id.first_row_button_1)
        Button button1;
        @BindView(R.id.first_row_button_2)
        Button button2;
        @BindView(R.id.first_row_button_3)
        Button button3;

        public FirstItemViewHolder(View convertView) {
            ButterKnife.bind(this, convertView);
        }
    }

    public static class SecondItemViewHolder {
        @BindView(R.id.button)
        Button button;

        public SecondItemViewHolder(View convertView) {
            ButterKnife.bind(this, convertView);
        }
    }
}
