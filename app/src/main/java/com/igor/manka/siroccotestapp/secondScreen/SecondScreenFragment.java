package com.igor.manka.siroccotestapp.secondScreen;


import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.ProgressBar;

import com.igor.manka.siroccotestapp.R;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;


public class SecondScreenFragment extends Fragment implements SecondListViewAdapter.SecondListViewAdapterButtonsListener {

    private static final int ITEMS_COUNT = 20;

    @BindView(R.id.secondScreenFragment_listView)
    ListView secondScreenFragmentListView;
    @BindView(R.id.progress_bar)
    ProgressBar progressBar;

    private SecondListViewAdapter secondListViewAdapter;
    private Unbinder unbinder;

    public SecondScreenFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_second_screen, container, false);
        unbinder = ButterKnife.bind(this, view);
        setUpList();
        return view;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @Override
    public void onButtonClick() {
        AsyncTaskRunner asyncTaskRunner = new AsyncTaskRunner();
        asyncTaskRunner.execute();
    }

    private void setUpList() {
        ArrayList<SecondScreenListModel> secondScreenListModels = new ArrayList<>();

        for (int i = 1; i <= ITEMS_COUNT; i++) {
            SecondScreenListModel secondScreenListModel;
            secondScreenListModel = new SecondScreenListModel();
            secondScreenListModel.setName(i + "");
            secondScreenListModel.setId(i);
            secondScreenListModel.setType(SecondListViewAdapter.NORMAL_ITEM_TYPE);
            if (i == 1) {
                secondScreenListModel.setType(SecondListViewAdapter.FIRST_ITEM_TYPE);
            } else if (i == 2) {
                secondScreenListModel.setType(SecondListViewAdapter.SECOND_ITEM_TYPE);
            } else if (i == 3) {
                secondScreenListModel.setType(SecondListViewAdapter.THIRD_ITEM_TYPE);
            }


            if (i % 3 == 0) {
                secondScreenListModel.setImage(R.drawable.android);
            } else if (i % 5 == 0) {
                secondScreenListModel.setImage(R.drawable.ios);
            } else {
                secondScreenListModel.setImage(R.drawable.windows);
            }
            secondScreenListModels.add(secondScreenListModel);
        }
        this.secondListViewAdapter = new SecondListViewAdapter(secondScreenListModels, getContext(), this);
        secondScreenFragmentListView.setAdapter(secondListViewAdapter);
    }

    private class AsyncTaskRunner extends AsyncTask<Void, Integer, String> {

        private ProgressDialog progressDialog;

        @Override
        protected String doInBackground(Void... voids) {
            int i = 0;
            while (i < 100) {
                try {
                    Thread.sleep(1000);
                    i += 20;
                    publishProgress(i);
                } catch (Exception e) {
                    Log.i("progressdialog", e.getMessage());
                }
            }
            return "COMPLETED";
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            progressDialog.dismiss();
        }

        @Override
        protected void onPreExecute() {
            progressDialog = new ProgressDialog(getContext());
            progressDialog.setTitle(null);
            progressDialog.setCancelable(false);
            progressDialog.setIndeterminate(false);
            progressDialog.setMax(100);
            progressDialog.setProgress(0);
            progressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
            progressDialog.show();
        }

        @Override
        protected void onProgressUpdate(Integer... integer) {
            super.onProgressUpdate(integer);
            progressDialog.setProgress(integer[0]);
        }
    }
}
