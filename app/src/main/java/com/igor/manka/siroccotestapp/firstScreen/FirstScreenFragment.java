package com.igor.manka.siroccotestapp.firstScreen;


import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.igor.manka.siroccotestapp.R;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnTouch;
import butterknife.Unbinder;

public class FirstScreenFragment extends Fragment implements FirstListViewAdapter.OnLoadingCompletedListener {

    @BindView(R.id.firstScreenFragment_headerTextView)
    TextView firstScreenFragmentHeaderTextView;
    @BindView(R.id.firstScreenFragment_header)
    RelativeLayout firstScreenFragmentHeader;
    @BindView(R.id.firstScreenFragment_listView)
    ListView firstScreenFragmentListView;

    Unbinder unbinder;
    private FirstListViewAdapter firstListViewsAdapter;
    private boolean flag_loading;

    public FirstScreenFragment() {
        // Required empty public constructor
    }

    @OnTouch(R.id.firstScreenFragment_headerTextView)
    public boolean onHeaderTextTouch() {
        handleOnHeaderTextTouch();
        return true;
    }

    @OnClick(R.id.firstScreenFragment_header)
    public void onHeaderClick() {
        handleOnHeaderClick();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_first_screen, container, false);
        unbinder = ButterKnife.bind(this, view);
        setUpList();
        return view;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @Override
    public void onLoadingCompleted() {
        flag_loading = false;
    }

    private void handleOnHeaderClick() {
        firstScreenFragmentHeaderTextView.setTextColor(ContextCompat.getColor(getContext(), R.color.white));
        AsyncTaskRunner asyncTaskRunner = new AsyncTaskRunner();
        asyncTaskRunner.execute();
    }

    private void handleOnHeaderTextTouch() {
        firstScreenFragmentHeaderTextView.setTextColor(ContextCompat.getColor(getContext(), R.color.red));
    }

    private void setUpList() {
        ArrayList<FirstScreenListModel> firstScreenListModels = new ArrayList<>();

        for (int i = 1; i < 101; i++) {
            FirstScreenListModel firstScreenListModel;
            firstScreenListModel = new FirstScreenListModel();
            firstScreenListModel.setName(i + "");
            firstScreenListModel.setType(FirstListViewAdapter.TYPE_NORMAL);
            if (i % 3 == 0) {
                firstScreenListModel.setImage(R.drawable.android);
            } else if (i % 5 == 0) {
                firstScreenListModel.setImage(R.drawable.ios);
            } else {
                firstScreenListModel.setImage(R.drawable.windows);
            }
            firstScreenListModels.add(firstScreenListModel);
        }

        this.firstListViewsAdapter = new FirstListViewAdapter(firstScreenListModels, this, getContext());
        firstScreenFragmentListView.setAdapter(firstListViewsAdapter);

        firstScreenFragmentListView.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {

            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                if (firstVisibleItem + visibleItemCount == totalItemCount && totalItemCount != 0) {
                    if (flag_loading == false) {
                        flag_loading = true;
                        firstListViewsAdapter.addItems();
                    }
                }
            }
        });
    }


    private class AsyncTaskRunner extends AsyncTask<Void, Integer, String> {

        private ProgressDialog progressDialog;

        @Override
        protected String doInBackground(Void... voids) {
            int i = 0;
            while (i < 100) {
                try {
                    Thread.sleep(1000);
                    i += 20;
                    publishProgress(i);
                } catch (Exception e) {
                    Log.i("progressdialog", e.getMessage());
                }
            }
            return "COMPLETED";
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            progressDialog.dismiss();
            firstScreenFragmentHeaderTextView.setTextColor(ContextCompat.getColor(getContext(), R.color.blue));
            firstScreenFragmentHeaderTextView.setEnabled(false);
        }

        @Override
        protected void onPreExecute() {
            progressDialog = new ProgressDialog(getContext());
            progressDialog.setTitle(null);
            progressDialog.setCancelable(true);
            progressDialog.setIndeterminate(false);
            progressDialog.setMax(100);
            progressDialog.setProgress(0);
            progressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
            progressDialog.setButton(DialogInterface.BUTTON_NEGATIVE, getString(R.string.cancel_progress_text), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                    AsyncTaskRunner.this.cancel(true);
                }
            });
            progressDialog.show();
        }

        @Override
        protected void onProgressUpdate(Integer... integer) {
            super.onProgressUpdate(integer);
            progressDialog.setProgress(integer[0]);
        }
    }
}
