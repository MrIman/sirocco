package com.igor.manka.siroccotestapp;

import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.widget.Toast;

import com.igor.manka.siroccotestapp.firstScreen.FirstScreenFragment;
import com.igor.manka.siroccotestapp.secondScreen.SecondScreenFragment;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MainActivity extends AppCompatActivity {

    @BindView(R.id.mainActivity_fragmentContainer)
    ViewPager mainActivityFragmentContainer;
    private boolean doubleBackPressedOnce;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        setUpView();
    }

    private void setUpView() {
        ButterKnife.bind(this);
        mainActivityFragmentContainer.setAdapter(new SiroccoPagerAdapter(getSupportFragmentManager()));
    }

    @Override
    public void onBackPressed() {
        if (doubleBackPressedOnce) {
            super.onBackPressed();
            return;
        }

        this.doubleBackPressedOnce = true;
        Toast.makeText(this, getString(R.string.back_pressed_once), Toast.LENGTH_SHORT).show();

        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                doubleBackPressedOnce = false;
            }
        }, 2000);
    }

    public static class SiroccoPagerAdapter extends FragmentPagerAdapter {
        private static int NUM_ITEMS = 2;

        public SiroccoPagerAdapter(FragmentManager fragmentManager) {
            super(fragmentManager);
        }

        @Override
        public int getCount() {
            return NUM_ITEMS;
        }

        @Override
        public Fragment getItem(int position) {
            switch (position) {
                case 0:
                    return new FirstScreenFragment();
                case 1:
                    return new SecondScreenFragment();
                default:
                    return null;
            }
        }
    }
}
