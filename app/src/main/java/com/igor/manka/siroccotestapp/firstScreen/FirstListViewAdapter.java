package com.igor.manka.siroccotestapp.firstScreen;

import android.content.Context;
import android.os.AsyncTask;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.igor.manka.siroccotestapp.R;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class FirstListViewAdapter extends ArrayAdapter<ArrayList<FirstScreenListModel>> {

    public static final int TYPE_NORMAL = 0;
    public static final int TYPE_LOADING = 1;
    private static final int MAX_ITEMS = 200;
    private final Context context;
    private final ArrayList<FirstScreenListModel> data;
    private final OnLoadingCompletedListener onLoadingCompletedListener;

    public FirstListViewAdapter(ArrayList<FirstScreenListModel> data, OnLoadingCompletedListener onLoadingCompletedListener, Context context) {
        super(context, R.layout.row_item_first_screen, Collections.singletonList(data));
        this.onLoadingCompletedListener = onLoadingCompletedListener;
        this.context = context;
        this.data = data;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        FirstScreenListModel firstScreenListModel = data.get(position);
        ViewHolder viewHolder;

        if (convertView == null) {
            LayoutInflater inflater = LayoutInflater.from(getContext());
            int itemViewType = getItemViewType(position);
            if (itemViewType == TYPE_LOADING) {
                convertView = inflater.inflate(R.layout.row_item_first_screen_loading, parent, false);
            } else {
                convertView = inflater.inflate(R.layout.row_item_first_screen, parent, false);
            }
            viewHolder = new ViewHolder(convertView);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }
        viewHolder.itemNumberTextView.setText("Item " + firstScreenListModel.getName());
        viewHolder.itemImage.setImageDrawable(ContextCompat.getDrawable(context, firstScreenListModel.getImage()));
        viewHolder.itemImage.setTag(position);


        return convertView;

    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public int getViewTypeCount() {
        return 2;
    }

    @Override
    public int getItemViewType(int position) {
        return data.get(position).getType();
    }

    public void addItems() {
        if (data.size() < MAX_ITEMS) {
            LoadMoreItemsAsyncTask loadMoreItemsAsyncTask = new LoadMoreItemsAsyncTask();
            loadMoreItemsAsyncTask.execute();
        }
    }

    public interface OnLoadingCompletedListener {
        void onLoadingCompleted();
    }

    public static class ViewHolder {

        @BindView(R.id.rowItemFirstScreen_image)
        ImageView itemImage;
        @BindView(R.id.rowItemFirstScreen_text)
        TextView itemNumberTextView;

        public ViewHolder(View convertView) {
            ButterKnife.bind(this, convertView);
        }
    }

    public class LoadMoreItemsAsyncTask extends AsyncTask<Void, Void, List<FirstScreenListModel>> {

        private static final long SLEEP_TIME = 5000;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            FirstScreenListModel firstScreenListModel;
            firstScreenListModel = new FirstScreenListModel();
            firstScreenListModel.setName(data.size() + "");
            firstScreenListModel.setImage(R.drawable.android);
            firstScreenListModel.setType(FirstListViewAdapter.TYPE_LOADING);
            data.add(firstScreenListModel);
            notifyDataSetChanged();
        }

        @Override
        protected List<FirstScreenListModel> doInBackground(Void... voids) {
            List<FirstScreenListModel> firstScreenListModels = new ArrayList<>();
            try {
                Thread.sleep(SLEEP_TIME);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            for (int i = data.size(); i < data.size() + 19; i++) {
                FirstScreenListModel firstScreenListModel;
                firstScreenListModel = new FirstScreenListModel();
                firstScreenListModel.setName(i + "");
                firstScreenListModel.setType(TYPE_NORMAL);
                if (i % 3 == 0) {
                    firstScreenListModel.setImage(R.drawable.android);
                } else if (i % 5 == 0) {
                    firstScreenListModel.setImage(R.drawable.ios);
                } else {
                    firstScreenListModel.setImage(R.drawable.windows);
                }
                firstScreenListModels.add(firstScreenListModel);
            }
            return firstScreenListModels;
        }

        @Override
        protected void onPostExecute(List<FirstScreenListModel> firstScreenListModels) {
            super.onPostExecute(firstScreenListModels);
            FirstScreenListModel lastModel = data.get(data.size() - 1);
            lastModel.setType(TYPE_NORMAL);
            data.set(data.size() - 1, lastModel);
            data.addAll(firstScreenListModels);
            notifyDataSetChanged();

            if (onLoadingCompletedListener != null) {
                onLoadingCompletedListener.onLoadingCompleted();
            }
        }
    }
}
